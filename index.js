var me = {};
me.avatar = "https://www.uzexpocentre.uz/uploads/manage/8fb42ce5f1daa471fd6577f22fcd5b71.png";

var you = {};
you.avatar = "https://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}


//-- No use time. It is a javaScript effect.
function insertChat(who, text, time){
      if(text!==""){
 
     
    var control = "";
    var date = formatAMPM(new Date());
    if (who == "me"){
          $.ajax({
            url: 'index.php', //This is the current doc
            type: "POST",
            data: ({name: text}),
            success: function(data){
               
            }
        });  
        control = '<li style="width:100%">' +
            '<div class="msj macro">' +
            '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ me.avatar +'" /></div>' +
            '<div class="text text-l">' +
            '<p>'+ text +'</p>' +
            '<p><small>'+date+'</small></p>' +
            '</div>' +
            '</div>' +
            '</li>';
    }else{
        control = '<li style="width:100%;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>'+text+'</p>' +
            '<p><small>'+date+'</small></p>' +
            '</div>' +
            '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="'+you.avatar+'" /></div>' +
            '</li>';
    }
    setTimeout(
        function(){
            $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
        }, time);
}
}

function resetChat(){
    $("ul").empty();
}
let olddata ="" ;
setInterval(function() {
    
    $.ajax({
  url: 'message.txt',
  dataType: 'text',
  
  success: function (data) {
    if(data==olddata)  
    {
        
    }else{
        olddata = data;
         insertChat(you, olddata, 0);
    }
  }
    });
}, 1000);
    

$(".mytext").on("keydown", function(e){


    if (e.which == 13){
        var text = $(this).val();
        if (text !== ""){
            insertChat("me", text);
            $(this).val('');
        }
    }
});

$('body > div > div > div:nth-child(2) > span').click(function(){
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
})

//-- Clear Chat
resetChat();

//-- Print Messages


//-- NOTE: No use time on insertChat.
